package varteq.zooar.domain;

import java.util.*;

public class Animal {
	private int id;
	private int zooId;
	private int animalTypeId;
	private String name;
	private int age;
	private String geodata;
	private String qrCode;
	private Date createDate;
	private Date updatedDate;
	private String status;
	
	public Animal(int id, int zooId) {
		this.id = id;
		this.zooId = zooId;
	}
	
	public int getId() {
		return id;
	}
	
	public int getZooId() {
		return zooId;
	}
}