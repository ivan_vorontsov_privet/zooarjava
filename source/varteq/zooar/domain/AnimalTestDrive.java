package varteq.zooar.domain;

public class AnimalTestDrive {
	public static void main(String[] args) {
		Animal a = new Animal(1, 2);
		int animalId = a.getId();
		int zooId = a.getZooId();
		System.out.println("Animal id : " + animalId + " has zooId : " + zooId);
	}
}